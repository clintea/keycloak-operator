#!/bin/bash
kubectl delete -f deploy/crds/lab_v1_keycloak_cr.yaml
kubectl delete -f deploy/operator.yaml
kubectl delete -f deploy/role_binding.yaml
kubectl delete -f deploy/role.yaml
kubectl delete -f deploy/service_account.yaml
kubectl delete -f deploy/crds/lab_v1_keycloak_crd.yaml
oc delete -f deploy/olm-catalog/keycloak-operator/0.0.1/keycloak-operator.v0.0.1.clusterserviceversion.yaml
