#!/bin/bash
kubectl create -f deploy/crds/lab_v1_keycloak_crd.yaml
operator-sdk --image-builder=buildah build registry.gitlab.com/clintea/keycloak-operator:v1
podman login -u${GITLAB_USER} -p${GITLAB_PASSWORD} registry.gitlab.com
podman push registry.gitlab.com/clintea/keycloak-operator:v1
kubectl create -f deploy/service_account.yaml
kubectl create -f deploy/role.yaml
kubectl create -f deploy/role_binding.yaml
kubectl create -f deploy/operator.yaml
oc apply -f deploy/olm-catalog/keycloak-operator/0.0.1/keycloak-operator.v0.0.1.clusterserviceversion.yaml
#kubectl apply -f deploy/crds/lab_v1_keycloak_cr.yaml
